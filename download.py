import requests
URL = open('URL.txt').read().split(',')

def download(url, pathname, filename):
    response = requests.get(url)

    with open(pathname + '/' + filename, "wb") as f:
        for chunk in response.iter_content():
            f.write(chunk)

for i in range(len(URL)):
  download.download(URL[i], 'images', str(i)+'.jpg')