import json
import download

annotations = json.load(open('annotations.json'))
dict_output = {}
licenses = []

dict = {0: 'Aluminium foil', 1: 'Battery', 2: 'Aluminium blister pack', 3: 'Carded blister pack',
        4: 'Other plastic bottle', 5: 'Clear plastic bottle', 6: 'Glass bottle', 7: 'Plastic bottle cap',
        8: 'Metal bottle cap', 9: 'Broken glass', 10: 'Food Can', 11: 'Aerosol', 12: 'Drink can', 13: 'Toilet tube',
        14: 'Other carton', 15: 'Egg carton', 16: 'Drink carton', 17: 'Corrugated carton', 18: 'Meal carton',
        19: 'Pizza box', 20: 'Paper cup', 21: 'Disposable plastic cup', 22: 'Foam cup', 23: 'Glass cup',
        24: 'Other plastic cup', 25: 'Food waste', 26: 'Glass jar', 27: 'Plastic lid', 28: 'Metal lid',
        29: 'Other plastic', 30: 'Magazine paper', 31: 'Tissues', 32: 'Wrapping paper', 33: 'Normal paper',
        34: 'Paper bag', 35: 'Plastified paper bag', 36: 'Plastic film', 37: 'Six pack rings', 38: 'Garbage bag',
        39: 'Other plastic wrapper', 40: 'Single-use carrier bag', 41: 'Polypropylene bag', 42: 'Crisp packet',
        43: 'Spread tub', 44: 'Tupperware', 45: 'Disposable food container', 46: 'Foam food container',
        47: 'Other plastic container', 48: 'Plastic glooves', 49: 'Plastic utensils', 50: 'Pop tab',
        51: 'Rope & strings', 52: 'Scrap metal', 53: 'Shoe', 54: 'Squeezable tube', 55: 'Plastic straw',
        56: 'Paper straw', 57: 'Styrofoam piece', 58: 'Unlabeled litter', 59: 'Cigarette'}

for image in annotations['images']:
    dict_output[image['id']] = {'image': image['flickr_url'], 'id': image['id'], 'width': image['width'],
                                'height': image['height']}

for annotation in annotations['annotations']:
    dict_output[annotation['image_id']].setdefault('category', []).append(dict[annotation['category_id']])
    dict_output[annotation['image_id']].setdefault('box', []).append(annotation['bbox'])

for image in dict_output:
    output = open('xml/' + str(image) + '.xml', 'w')
    output.write('''<annotation>
  <folder/>
  <filename>''' + str(dict_output[image]['id']) + '''.jpg</filename>
  <source>
    <database>Unknown</database>
    <annotation>Unknown</annotation>
    <image>Unknown</image>
  </source>
  <size>
    <width>''' + str(dict_output[image]['height']) + '''</width>
    <height>''' + str(dict_output[image]['width']) + '''</height>
    <depth>3</depth>
  </size>
  <segmented>0</segmented>''')
    for i in range(len(dict_output[image]['category'])):
        output.write('''
  <object>
    <name>''' + str(dict_output[image]['category'][i]) + '''</name>
    <bndbox>
      <xmin>''' + str(dict_output[image]['box'][i][0]) + '''</xmin>
      <ymin>''' + str(dict_output[image]['box'][i][1]) + '''</ymin>
      <xmax>''' + str(dict_output[image]['box'][i][0] + dict_output[image]['box'][i][2]) + '''</xmax>
      <ymax>''' + str(dict_output[image]['box'][i][1] + dict_output[image]['box'][i][3]) + '''</ymax>
    </bndbox>
  </object>''')
    output.write('''
</annotation>''')
    output.close()
